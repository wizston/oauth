<?php namespace Cerbero\Oauth\Providers;

/**
 * Interface for OAuth providers.
 *
 * @author	Andrea Marco Sartori
 */
interface ProviderInterface
{

	/**
	 * Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array|null	$scopes
	 * @return	string
	 */
	public function getAuthUrl($scopes = null);

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function storeAccessToken();

	/**
	 * Return the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getClient();

	/**
	 * Retrieve the unique user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getID();

	/**
	 * Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	public function getEmail();

	/**
	 * Call services dynamically.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$name
	 * @param	array|null	$arguments
	 * @return	mixed
	 */
	public function hookService($name, $arguments = null);

}