<?php namespace Cerbero\Oauth\Providers;

/**
 * Facebook provider.
 *
 * @author	Andrea Marco Sartori
 */
class Facebook extends AbstractProvider implements ProviderInterface
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		\BaseFacebook	$client	The Facebook client.
	 */
	protected $client;

	/**
	 * Set the Facebook client.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	\BaseFacebook	$client
	 * @return	void
	 */
	public function __construct(\BaseFacebook $client)
	{
		$this->client = $client;
	}

	/**
	 * Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array|null	$scopes
	 * @return	string
	 */
	public function getAuthUrl($scopes = null)
	{
		return $this->client->getLoginUrl(array
		(
			'scope' => $scopes,

			'redirect_uri' => $this->getRedirectUri()
		));
	}

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function storeAccessToken()
	{
		$this->client->getAccessToken();
	}

	/**
	 * Return the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Retrieve the unique user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getID()
	{
		return $this->client->getUser();
	}

	/**
	 * Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	public function getEmail()
	{
		$user = $this->client->api('/me');

		if(is_array($user)) return $user['email'];

		throw new \UnexpectedValueException('Email is not provided by the given scopes.');
	}

}