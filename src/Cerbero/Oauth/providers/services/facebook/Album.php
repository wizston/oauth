<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

/**
 * Service for albums.
 *
 * @author	Andrea Marco Sartori
 */
class Album extends AbstractFacebookService
{

	/**
	 * Retrieve an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function get()
	{
		return parent::get();
	}

	/**
	 * Retrieve all photos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function photos()
	{
		return parent::photos();
	}

	/**
	 * Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function likes()
	{
		return parent::likes();
	}

	/**
	 * Like an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function like()
	{
		return parent::like();
	}

	/**
	 * Retrieve all comments.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function comments()
	{
		return parent::comments();
	}

	/**
	 * Add a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function comment($message)
	{
		return parent::comment($message);
	}

	/**
	 * Create an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$name
	 * @param	string|null	$message
	 * @param	array	$privacy
	 * @return	int
	 */
	public function create($name, $message = '', $privacy = 'all')
	{
		$privacy = $this->getPrivacy($privacy);

		$id = head($this->api('me/albums', 'POST', compact('name', 'message', 'privacy')));

		$this->attachPhotos($id);

		return $id;
	}

	/**
	 * Add a photo.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string|null	$message
	 * @return	int
	 */
	public function addPhoto($source, $message = '')
	{
		return parent::addPhoto($source, $message);
	}

	/**
	 * Allow photo addition via method chaining.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$message
	 * @return	Cerbero\Oauth\Providers\Services\Facebook\Album	$this
	 */
	public function with($source, $title = '', $description = '')
	{
		return parent::with($source, $title);
	}

}