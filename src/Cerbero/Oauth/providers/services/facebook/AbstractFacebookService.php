<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

use Cerbero\Oauth\Providers\Services\AbstractService,
	Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Facebook service abstraction.
 *
 * @author	Andrea Marco Sartori
 */
abstract class AbstractFacebookService extends AbstractService
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$photos	Photos to add to a resource.
	 */
	protected $photos = array();

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$videos	Video to add to a resource.
	 */
	protected $videos = array();

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$allowedVideos	Video extensions allowed.
	 */
	protected $allowedVideos = array('3g2', '3gp', '3gpp', 'asf', 'avi', 'dat', 'divx', 'dv', 'f4v', 'flv',
									'm2ts', 'm4v', 'mkv', 'mod', 'mov', 'mp4', 'mpe', 'mpeg', 'mpeg4', 
									'mpg', 'mts', 'nsv', 'ogm', 'ogv', 'qt', 'tod', 'ts', 'vob', 'wmv');

	/**
	 * Retrieve a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	protected function get()
	{
		$id = $this->getAttributes();

		return $this->api($id);
	}

	/**
	 * Shorthand to perform API calls.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$path
	 * @param	string	$method
	 * @param	array	$params
	 * @return	mixed
	 */
	protected function api($path, $method = 'GET', $params = array())
	{
		return $this->client()->api($path, $method, $params);
	}

	/**
	 * Retrieve the correct privacy.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$alias
	 * @return	array
	 */
	protected function getPrivacy($alias)
	{
		if(is_array($alias)) return $alias;

		$privacy = $this->replacePrivacy($alias);

		return array('value' => $privacy);
	}

	/**
	 * Replace alias with the correct privacy value.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$alias
	 * @return	string
	 */
	protected function replacePrivacy($alias)
	{
		$aliases = array('all', 'fof', 'friends', 'me');

		$values = array('EVERYONE', 'FRIENDS_OF_FRIENDS', 'ALL_FRIENDS', 'SELF');

		return str_replace($aliases, $values, $alias);
	}

	/**
	 * Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function likes()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/likes");
	}

	/**
	 * Like a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	protected function like()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/likes", 'POST');
	}

	/**
	 * Dislike a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	protected function dislike()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/likes", 'DELETE');
	}

	/**
	 * Retrieve all comments.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function comments()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/comments");
	}

	/**
	 * Add a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function comment($message)
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/comments", 'POST', compact('message')));
	}

	/**
	 * Remove a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	int
	 */
	protected function remove()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}", 'DELETE');
	}

	/**
	 * Update a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$params
	 * @return	boolean
	 */
	protected function update($params)
	{
		$id = $this->getAttributes();

		return $this->api("{$id}", 'POST', $params);
	}

	/**
	 * Retrieve all posts.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function posts()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/feed");
	}

	/**
	 * Add a post to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$params
	 * @return	int
	 */
	protected function post($params)
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/feed", 'POST', $params));
	}

	/**
	 * Retrieve the picture.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$type
	 * @param	boolean	$redirect
	 * @return	mixed
	 */
	protected function picture($type, $redirect = true)
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/picture", 'GET', compact('type', 'redirect'));
	}

	/**
	 * Set the picture.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$param
	 * @return	AbstractFacebookService
	 */
	protected function setPicture($param)
	{
		$id = $this->getAttributes();

		$picture = $this->getUrlOrSource($param);

		$this->api("{$id}/picture", 'POST', $picture);

		return $this;
	}

	/**
	 * Retrieve either the URL or source.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$param
	 * @return	array
	 */
	protected function getUrlOrSource($param)
	{
		if($param instanceof UploadedFile)
		{
			return array('source' => $this->processSource($param));
		}
		return array('url' => $param);
	}

	/**
	 * Process the source.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @return	array
	 */
	protected function processSource(UploadedFile $source)
	{
		return curl_file_create($source->getRealPath(), $source->getMimeType());
	}

	/**
	 * Retrieve all photos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function photos()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/photos");
	}

	/**
	 * Add a photo.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string|null	$message
	 * @return	int
	 */
	protected function addPhoto($source, $message = '')
	{
		$id = $this->getAttributes();

		$this->setPhotos($source, $message);

		return $this->attachPhotos($id);
	}

	/**
	 * Set photos to add.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$message
	 * @return	void
	 */
	protected function setPhotos($source, $message)
	{
		$photo = $this->getUrlOrSource($source);

		$this->photos[] = $photo + compact('message');
	}

	/**
	 * Attach photos to a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	int	$id
	 * @return	int
	 */
	protected function attachPhotos($id)
	{
		while ($this->photos)
		{
			$photo = array_shift($this->photos);

			$id = head($this->api("{$id}/photos", 'POST', $photo));
		}
		return $id;
	}

	/**
	 * Allow media addition via method chaining.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$title
	 * @param	string	$description
	 * @return	AbstractFacebookService
	 */
	protected function with($source, $title = '', $description = '')
	{
		$this->setMedia($source, $title, $description);

		return $this;
	}

	/**
	 * Set different kind of media.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$title
	 * @param	string	$description
	 * @return	void
	 */
	protected function setMedia($source, $title, $description)
	{
		$ext = $source->guessExtension();

		if(in_array($ext, $this->allowedVideos))
		{
			return $this->setVideos($source, $title, $description);
		}
		$this->setPhotos($source, $title);
	}

	/**
	 * Set videos to add.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$title
	 * @param	string	$description
	 * @return	void
	 */
	protected function setVideos($source, $title, $description)
	{
		$video = array('source' => $this->processSource($source));

		$this->videos[] = $video + compact('title', 'description');
	}

	/**
	 * Attach videos to a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	int	$id
	 * @return	int
	 */
	protected function attachVideos($id)
	{
		while ($this->videos)
		{
			$video = array_shift($this->videos);

			$id = head($this->api("{$id}/videos", 'POST', $video));
		}
		return $id;
	}

	/**
	 * Attach media to a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	int	$id
	 * @return	void
	 */
	protected function attachMedia($id)
	{
		$this->attachPhotos($id);

		$this->attachVideos($id);
	}

	/**
	 * Retrieve all videos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function videos()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/videos");
	}

	/**
	 * Add a video.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$title
	 * @param	string	$description
	 * @return	int
	 */
	protected function addVideo($source, $title = '', $description = '')
	{
		$id = $this->getAttributes();

		$source = $this->processSource($source);

		return head($this->api("{$id}/videos", 'POST', compact('source', 'title', 'description')));
	}

}