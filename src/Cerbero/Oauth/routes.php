<?php

$uri = Config::get('oauth::_settings.redirect_uri');

Route::get('login/oauth/{provider}', array('before' => 'csrf.oauth', function($provider) use($uri)
{
	App::make("oauth.{$provider}")->authorize();

	return Redirect::to($uri);
}));